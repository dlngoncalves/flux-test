using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

[System.Serializable]
public class GameState
{
    //We will take the data from the Statics class, as I Believe thats where current game state is stored.

    public int ShootingSelected;
    public bool WithShield;
    public int Damage;//not sure if needed
    public int SpeedMultiplier;
    public int CurrentLevel;
    public int EnemiesDead;
    public float Life;
    public float ShieldLife;
    public int Points;

    //just have a basic constructor to fill everything
    public GameState(int shootingSelected, bool withShield, int damage, int speedMultiplier, int currentLevel, int enemiesDead, float life, int points,float shieldLife)
    {
        ShootingSelected = shootingSelected;
        WithShield = withShield;
        Damage = damage;
        SpeedMultiplier = speedMultiplier;
        CurrentLevel = currentLevel;
        EnemiesDead = enemiesDead;
        Life = life;
        Points = points;
        ShieldLife = shieldLife;
    }

    //another constructor in case the player dies, then just return them to the level
    //removed WithShield from this case because we are always starting with a shield
    public GameState(int shootingSelected, int damage, int speedMultiplier, int currentLevel)
    {
        ShootingSelected = shootingSelected;
        Damage = damage;
        SpeedMultiplier = speedMultiplier;
        CurrentLevel = currentLevel;
    }

    //there are more things that could be saved, like the player position, or amount of enemies onscreen.
    //public Vector2 PlayerPosition;







}
