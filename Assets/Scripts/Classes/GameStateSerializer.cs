using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using Gaminho;

public static class GameStateSerializer
{
    public static GameState savedState=null;
    public static GameState limitedState=null;//for player death

    public static void SaveGameState()
    {   //I Could just have serialized the statics class I guess
        savedState = null;//erase the last saved game
        savedState = new GameState(Statics.ShootingSelected, Statics.WithShield, Statics.Damage, Statics.SpeedMultiplier, Statics.CurrentLevel, Statics.EnemiesDead, Statics.Life, Statics.Points,Statics.ShieldLife);

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/savedgame.gd");
        bf.Serialize(file, GameStateSerializer.savedState);
        file.Close();//and we are done saving
    }

    public static void SaveGameStateDeath()
    {   //I Could just have serialized the statics class I guess
        limitedState = null;//erase the last saved game
        limitedState = new GameState(Statics.ShootingSelected, Statics.Damage, Statics.SpeedMultiplier, Statics.CurrentLevel);

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/savedeath.gd");
        bf.Serialize(file, GameStateSerializer.limitedState);
        file.Close();//and we are done saving
    }


    //not very apparent from function names that we still have to set the values in the Statics class...

    public static void LoadGameState()
    {
        if(File.Exists(Application.persistentDataPath + "/savedgame.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/savedgame.gd",FileMode.Open);
            GameStateSerializer.savedState = (GameState)bf.Deserialize(file);
            file.Close();//and we are done loading
        }
    }

    public static void LoadGameStateDeath()
    {
        if (File.Exists(Application.persistentDataPath + "/savedeath.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/savedeath.gd", FileMode.Open);
            GameStateSerializer.limitedState = (GameState)bf.Deserialize(file);
            file.Close();//and we are done loading
        }
    }

}
