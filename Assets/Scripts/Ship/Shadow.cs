using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gaminho;
using UnityEngine.UI;
using UnityEngine.Events;

public struct FrameData
{
    public Vector2 position;
    public Quaternion rotation;
    public bool shooting;
    public bool motorState;
    public bool shieldActive;
}

public class Shadow : MonoBehaviour
{
    [Range(0.1f, 5.0f)]
    public float ShadowDelay;
    public Queue<FrameData> shipPositions = new Queue<FrameData>();
    public Shot[] Shots;
    public GameObject MotorAnimation;
    public GameObject Shield;

    private FrameData currentFrame;
    private Image[] sprites;

    // Start is called before the first frame update
    void Start()
    {
        sprites = GetComponentsInChildren<Image>();
        foreach(Image curSprite in sprites)
        {
            curSprite.enabled = false;
        }

        StartCoroutine(ShadowCaller());
    }

    private IEnumerator ShadowCaller()
    {
        yield return new WaitForSeconds(ShadowDelay);

        foreach (Image curSprite in sprites)
        {
            curSprite.enabled = true;
        }

        yield return StartCoroutine(MoveShadow());
    }

    private IEnumerator MoveShadow()
    {
        while (true)
        {
            if (shipPositions.Count > 0)//msft doc says there should be a TryDequeue method that tests for empty. couldnt find it.
            {
                currentFrame = shipPositions.Dequeue();
            }

            transform.localPosition = currentFrame.position;
            transform.localRotation = currentFrame.rotation;

            //if (currentFrame.shooting)//shooting is experimental
            //{
            //    Shoot();//probably should have a cooldown for the shadow shots
            //}

            if (currentFrame.motorState)
            {
                MotorAnimation.SetActive(true);
            }
            else
            {
                MotorAnimation.SetActive(false);
            }

            if (currentFrame.shieldActive)
            {
                Shield.SetActive(true);
            }
            else
            {
                Shield.SetActive(false);
            }

            yield return null;
        }
    }

    //just copying the shooting from the player
    private void Shoot()
    {
        //removing all damage, just ghosting the shots
        Statics.Damage = Shots[Statics.ShootingSelected].Damage;
        GameObject goShoot = Instantiate(Shots[Statics.ShootingSelected].Prefab, Vector3.zero, Quaternion.identity);
        goShoot.transform.parent = transform;
        goShoot.transform.localPosition = Shots[Statics.ShootingSelected].Weapon.transform.localPosition;
        goShoot.GetComponent<Rigidbody2D>().AddForce(transform.up * ((Shots[Statics.ShootingSelected].SpeedShooter * 12000f) * Time.deltaTime), ForceMode2D.Impulse);
        goShoot.AddComponent<BoxCollider2D>();
        goShoot.transform.parent = transform.parent;
        goShoot.tag = "ShadowShot";
        if (Shots[Statics.ShootingSelected].TypeShooter == Statics.TYPE_SHOT.DOUBLE)
        {
            GameObject goShoot2 = Instantiate(Shots[Statics.ShootingSelected].Prefab, Vector3.zero, Quaternion.identity);
            goShoot2.transform.parent = transform;
            goShoot2.transform.localPosition = Shots[Statics.ShootingSelected].Weapon2.transform.localPosition;
            goShoot2.GetComponent<Rigidbody2D>().AddForce(transform.up * ((Shots[Statics.ShootingSelected].SpeedShooter * 12000f) * Time.deltaTime), ForceMode2D.Impulse);
            goShoot2.AddComponent<BoxCollider2D>();
            goShoot2.transform.parent = transform.parent;
            goShoot.tag = "ShadowShot";
        }

        if (Shots[Statics.ShootingSelected].TypeShooter == Statics.TYPE_SHOT.TRIPLE)
        {
            GameObject goShoot2 = Instantiate(Shots[Statics.ShootingSelected].Prefab, Vector3.zero, Quaternion.identity);
            goShoot2.transform.parent = transform;
            goShoot2.transform.localPosition = Shots[Statics.ShootingSelected].Weapon2.transform.localPosition;
            goShoot2.GetComponent<Rigidbody2D>().AddForce(transform.up * ((Shots[Statics.ShootingSelected].SpeedShooter * 12000f) * Time.deltaTime), ForceMode2D.Impulse);
            goShoot2.AddComponent<BoxCollider2D>();
            goShoot2.transform.parent = transform.parent;
            goShoot2.tag = "ShadowShot";

            GameObject goTiro3 = Instantiate(Shots[Statics.ShootingSelected].Prefab, Vector3.zero, Quaternion.identity);
            goTiro3.transform.parent = transform;
            goTiro3.transform.localPosition = Shots[Statics.ShootingSelected].Weapon3.transform.localPosition;
            goTiro3.GetComponent<Rigidbody2D>().AddForce(transform.up * ((Shots[Statics.ShootingSelected].SpeedShooter * 12000f) * Time.deltaTime), ForceMode2D.Impulse);
            goTiro3.AddComponent<BoxCollider2D>();
            goTiro3.transform.parent = transform.parent;
            goTiro3.tag = "ShadowShot";
        }
    }

}
