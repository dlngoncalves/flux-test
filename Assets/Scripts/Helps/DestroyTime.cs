﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gaminho;

public class DestroyTime : MonoBehaviour {

    public float delay = 2f; // in Secounds


    private void Start()
    {
        StartCoroutine(CallDestroy());
    }

    private IEnumerator CallDestroy()
    {
        yield return new WaitForSeconds(delay);
        if (gameObject)
        {
            if (gameObject.tag == "Shot")
            {
                Statics.ActivePlayerShots--;
            }
            if(gameObject.tag == "Enemy")
            {
                if(gameObject.GetComponent<Enemy>().Neo == true)//I know getcomponent bad
                {
                    Enemy.ThereIsOne = false;
                }
            }
            Destroy(gameObject);
        }
    }
}
