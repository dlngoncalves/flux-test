﻿using Gaminho;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CallScene : MonoBehaviour {


    public void Call(string sname)
    {
        GameObject.Instantiate(Resources.Load(Statics.PREFAB_LOAD) as GameObject);
        SceneManager.LoadScene(sname);

    }

    public void ContinueAfterDeath(string sname)
    {
        //think we might be saving twice in case of death
        Statics.LoadedGame = false;//in this case we "load" but "reset" the level
        Statics.Points = 0;//think points were being reset somewhere else. When loading the death save from disk points were reset correctly already.
        GameStateSerializer.SaveGameStateDeath();
        GameObject.Instantiate(Resources.Load(Statics.PREFAB_LOAD) as GameObject);
        SceneManager.LoadScene(sname);
    }
}