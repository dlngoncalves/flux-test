﻿using Gaminho;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;

public class ControlStart : MonoBehaviour {
    public Text Record;
    public GameObject BtnContinue;
	// Use this for initialization
	void Start () {
        
        //Decided to check here if a save game exists so otherwise we disable the button... but kept the checks in the load function
        if(!(File.Exists(Application.persistentDataPath + "/savedgame.gd")) && !(File.Exists(Application.persistentDataPath + "/savedeath.gd")))
        {
            BtnContinue.SetActive(false);
        }
            
        Statics.WithShield = true;//changed to start with shield
        Statics.EnemiesDead = 0;
        Statics.CurrentLevel = 0;
        Statics.Points = 0;
        Statics.ShootingSelected = 2;
        //Loads Record
        if (PlayerPrefs.GetInt(Statics.PLAYERPREF_VALUE) == 0)
        {
            PlayerPrefs.SetString(Statics.PLAYERPREF_NEWRECORD, "Nobody");
        }
        Record.text = "Record: " + PlayerPrefs.GetString(Statics.PLAYERPREF_NEWRECORD) + "(" + PlayerPrefs.GetInt(Statics.PLAYERPREF_VALUE) + ")";
       
	}

	public void StartClick()
    {
#if !UNITY_EDITOR
    Debug.Log("Σφάλμα σκόπιμα, το βρήκατε, συγχαρητήρια!");
    //CRUX SACRA SIT MIHI LUX 
    //NON DRACO SIT MIHI DUX
    //VADE RETRO SATANA 
    //NUNQUAM SUADE MIHI VANA
    //SUNT MALA QUAE LIBAS
    //IPSE VENENA BIBAS
    //Sair();
    //return;
#endif
        //if the playerclicks start its game over, we destroy the saves
        GameStateSerializer.savedState = null;
        File.Delete(Application.persistentDataPath + "/savedgame.gd");

        GameStateSerializer.limitedState = null;
        File.Delete(Application.persistentDataPath + "/savedeathgd.gd");

        Statics.LoadedGame = false;

        GetComponent<AudioSource>().Stop();
        //Something is wrong in this history prefab that the play button can be out of the screen depending on the res.
        //thing might be the anchor point + position
        //also was behind the video?
        //fixed by moving the button around, but I'm not sure it was supposed to appear at the beginning.
        GameObject.Instantiate(Resources.Load(Statics.PREFAB_HISTORY) as GameObject);
    }

    //for the player to resume
    //we are going to assume the player always wants to restart from the complete save(points, enemies killed are preserved)
    //so we look for the complete file first, if cant find it we look for limited
    public void LoadClick()
    {
        //this is done also in the load function, but I need it here
        if (File.Exists(Application.persistentDataPath + "/savedgame.gd"))
        {
            GameStateSerializer.LoadGameState();
            Statics.ShootingSelected = GameStateSerializer.savedState.ShootingSelected;
            Statics.WithShield = GameStateSerializer.savedState.WithShield;
            Statics.Damage = GameStateSerializer.savedState.Damage;
            Statics.SpeedMultiplier = GameStateSerializer.savedState.SpeedMultiplier;
            Statics.CurrentLevel = GameStateSerializer.savedState.CurrentLevel;
            Statics.EnemiesDead = GameStateSerializer.savedState.EnemiesDead;
            Statics.Life = GameStateSerializer.savedState.Life;
            Statics.Points = GameStateSerializer.savedState.Points;
            Statics.ShieldLife = GameStateSerializer.savedState.ShieldLife;
            Statics.LoadedGame = true;
        }
        else if (File.Exists(Application.persistentDataPath + "/savedeath.gd"))
        {
            GameStateSerializer.LoadGameStateDeath();
            Statics.ShootingSelected = GameStateSerializer.limitedState.ShootingSelected;
            Statics.Damage = GameStateSerializer.limitedState.Damage;
            Statics.SpeedMultiplier = GameStateSerializer.limitedState.SpeedMultiplier;
            Statics.CurrentLevel = GameStateSerializer.limitedState.CurrentLevel;
            Statics.EnemiesDead = 0;
            Statics.Points = 0;
            Statics.WithShield = true; //give the shield back
            Statics.LoadedGame = false;//in this case we "load" but "reset" the level
        }
        else
        {
            return; //nothing to see here
        }

        
        //continuing with the show
        GetComponent<AudioSource>().Stop();
        //wanted to keep all scene calls in the same script but I think in the loading case it might mess up with some of the statics
        GameObject.Instantiate(Resources.Load(Statics.PREFAB_LOAD) as GameObject);
        SceneManager.LoadScene("Game");
    }

    public void Quit()
    {
        Application.Quit();
    }

    
}
