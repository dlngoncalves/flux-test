using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Gaminho;

public class UIControl : MonoBehaviour
{
    //I Prefer to have a centralized Script to have all the callbacks to the UI Buttons and attach that script to the Canvas object
    //Don't know if that is THE BEST practice, but saves some time.
    //Just have to be careful with how complex the functions you keep here are, because things can get messy.
    //Ideally you just send messages/call controllers from here
    //For now, just using for the Pause Button, so the pause logic stays here.

    private Statics.RUNNING_STATUS currentStatus = Statics.RUNNING_STATUS.RUNNING; //we start running
    private AudioSource[] allSceneAudios;
    public Text BtnText;

    //Realized passing a new status was unnecessary if we keep the current status as this is simply a toggle switch. also was going to have to add a listener to the button.
    //public void ChangePauseStatus(Statics.RUNNING_STATUS newStatus)
    public void ChangePauseStatus()

    {
        //decided against using this as after the swap the name of the variable became ambiguous
        //we currently are running, but the currentStatus is paused, but also we have to perform pausing actions
        //keeping this here to show sometimes my first instinct is to create something more complicated then what is needed
        //But I usually try to simplify things
        //
        //first we change
        //if(currentStatus == Statics.RUNNING_STATUS.RUNNING)
        //{
        //    currentStatus = Statics.RUNNING_STATUS.PAUSED;
        //}
        //else if(currentStatus == Statics.RUNNING_STATUS.PAUSED)
        //{
        //    currentStatus = Statics.RUNNING_STATUS.RUNNING;
        //}

        //if currentstatus is Running, we pause (and change the status), and vice-versa. more simple
        switch (currentStatus)
        {
            case Statics.RUNNING_STATUS.RUNNING:
                Time.timeScale = 0;
                //caching here at least we avoid ONE FindObjectsOfType
                allSceneAudios = GameObject.FindObjectsOfType<AudioSource>();
                //I KNOW i've done something like this on a list or array in c# without a for loop (maybe a lambda), but couldnt find it.
                //Dont want to use LINQ. In C++ could use std::transform + a lambda.
                foreach (AudioSource source in allSceneAudios)
                {
                    source.Stop();
                }
                currentStatus = Statics.RUNNING_STATUS.PAUSED;
                BtnText.text = "Resume";
                break;
            case Statics.RUNNING_STATUS.PAUSED:
                Time.timeScale = 1;
                foreach (AudioSource source in allSceneAudios)
                {
                    if(source.gameObject.name == "Control")//special case for the music
                    {
                        AudioClip currentSong = source.gameObject.GetComponent<ControlGame>().Levels[Statics.CurrentLevel].AudioLvl;
                        source.PlayOneShot(currentSong);
                    }
                    else
                    {
                        source.Play();
                    }
                }
                currentStatus = Statics.RUNNING_STATUS.RUNNING;
                BtnText.text = "Pause";
                break;
            default:
                break;
        }
    }
}
