﻿using Gaminho;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlGame : MonoBehaviour {
    public ScenarioLimits ScenarioLimit;
    public Level[] Levels;
    public Image Background;
    [Header("UI")]
    public Text TextStart;
    public Text TextPoints;
    public Transform BarLife;
    public Transform BarShield;

    [SerializeField]
    [Range(1, 10)]
    public int EnemyShootingSpeedMultiplier;

    // Use this for initialization
    void Start () {

        if (!Statics.LoadedGame) 
        {//just making sure we dont overide the complete savegame
            Statics.EnemiesDead = 0;
            Statics.SpeedMultiplier = EnemyShootingSpeedMultiplier;
        }

        Background.sprite = Levels[Statics.CurrentLevel].Background;
        TextStart.text = "Stage " + (Statics.CurrentLevel + 1);
        GetComponent<AudioSource>().PlayOneShot(Levels[Statics.CurrentLevel].AudioLvl);
        
    }

    private void Update()
    {
        TextPoints.text = Statics.Points.ToString();
        //changed here because if you increased the life the bar would grow out of proportion.
        //How did I figure this out? Well I'm bad at this game so I gave myself 10000 life to test it. Not proud of it.
        BarLife.localScale = new Vector3(Statics.Life / 10f, 1, 1);
        BarShield.localScale = new Vector3(Statics.ShieldLife / 10f, 1, 1);
    }

    public void LevelPassed()
    {
        
        Clear();
        Statics.CurrentLevel++;
        Statics.Points += 1000 * Statics.CurrentLevel;
        if (Statics.CurrentLevel < 3)
        {
            GameObject.Instantiate(Resources.Load(Statics.PREFAB_LEVELUP) as GameObject);
        }
        else
        {
            GameObject.Instantiate(Resources.Load(Statics.PREFAB_CONGRATULATION) as GameObject);
        }
        GameStateSerializer.SaveGameState();//autosave
    }
    //Oops, when you lose (: Starts from Zero
    public void GameOver()
    {
        BarLife.localScale = new Vector3(0, 1, 1);
        BarShield.localScale = new Vector3(0, 1, 1);
        Clear();
        GameStateSerializer.SaveGameStateDeath();
        Destroy(Statics.Player.gameObject);
        GameObject.Instantiate(Resources.Load(Statics.PREFAB_GAMEOVER) as GameObject);
    }
    private void Clear()
    {
        GetComponent<AudioSource>().Stop();
        GameObject[] Enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject ini in Enemies)
        {
            Destroy(ini);
        }
    }

    //we always save on quit
    public void SaveGame()
    {
        GameStateSerializer.SaveGameState();
    }
}
