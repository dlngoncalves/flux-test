﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gaminho;
using System;
using UnityEngine.Events;


public class Enemy : MonoBehaviour {

    public Statics.TYPE_ENEMY MyType;
    public Life _life;
    public GameObject Explosion;
    public int Damage = 1;
    public Shot shot;
    public GameObject ItemDrop;
    public static bool ThereIsOne = false;
    public bool Neo;//technically he doesn't need to dodge bullets...
    [Range(0, 1000)]
    public float DistanceToAvoid = 100.0f;
    public UnityEvent Dodge;
    public ControlGame Control;

    // Use this for initialization
    void Start () {
        if (MyType == Statics.TYPE_ENEMY.SHIP)
        {
            StartCoroutine(Shoot());
        }

        //never a fan of Find
        GameObject ControlObj = GameObject.Find("Control");
        if(ControlObj != null)
        {
            Control = ControlObj.GetComponent<ControlGame>();
        }//should have an error case
    }

    private void Update()
    {
       
        if (!Statics.Player) return;
        if(MyType == Statics.TYPE_ENEMY.SHIP)
        {

            Quaternion q = Statics.FaceObject(Statics.Player.localPosition, transform.localPosition, Statics.FacingDirection.Right);
            transform.rotation = q;
        }
    }

    private void LateUpdate()
    {
        if (Neo && MyType == Statics.TYPE_ENEMY.SHIP && Statics.ActivePlayerShots > 0)
        {
            Dodge.Invoke();//in the end there was no need to use an event
        }
    }

    public void DodgeThis()
    {
        //here I should have really just cached the active shots on the player object and looked. will change if theres time.
        //But, from stackoverflow:
        //FindGameObjectWithTag() has a complexity O(n) in the worst case scenario.
        //You will not have performance issue if you have small amount of objects in your scene
        //and small amount of objects searching for objects with tags.

        //this should be a class member
        GameObject[] currentPlayerShots;

        //this means the player fired again! or maybe one of his shots got destroyed.
        //But in this case we look for the actual player shots to dodge
        //all of this is to avoid calling FindGameObjectsWithTaga every fixed frame.
        
        //should probably cache the shots directly on the player to avoid this
        currentPlayerShots = GameObject.FindGameObjectsWithTag("Shot");

        float smallest = Mathf.Infinity;
        GameObject shotToAvoid = null;
        foreach(GameObject shot in currentPlayerShots)
        {
            //if (shot.transform.position.x < Control.ScenarioLimit.xMin || shot.transform.position.x > Control.ScenarioLimit.xMax)
            //    break;
            //if (shot.transform.position.y < Control.ScenarioLimit.yMin || shot.transform.position.y > Control.ScenarioLimit.yMax)
            //    break;

            float distance = Vector2.Distance(transform.position, shot.transform.position);
            if(distance < DistanceToAvoid && distance < smallest)
            {
                smallest = distance;
                shotToAvoid = shot;
            }
            //want to see if we can see if we are facing the object
            //float direction = Vector2.Dot(transform.position.normalized, shot.transform.position.normalized)
           
        }

        if (shotToAvoid != null)
        {
            //trying to face away from the shot, not working very well
            Quaternion q = Statics.FaceObject(-shotToAvoid.transform.localPosition, transform.localPosition, Statics.FacingDirection.Right);
            transform.rotation = q;

            MoveEnemy(transform.localPosition - shotToAvoid.transform.localPosition);
        }

    }

    
    private void MoveEnemy(Vector2 direction)
    {
        //the direction of movement isnt very good, should make it perpendicular to the shot
        //as we are just pushing away now, it can move several times and look weird
        //but it works
        Rigidbody2D body = GetComponent<Rigidbody2D>();
        direction = direction.normalized;
        float force = 100f * body.mass;//using same force as when created, might be too much
        body.AddForce(direction * force, ForceMode2D.Impulse);
        
        StartCoroutine(ResetShipForce());
    }

    private IEnumerator ResetShipForce()
    {
        //should cache all those waitforseconds
        yield return new WaitForSeconds(0.2f);
        Rigidbody2D body = GetComponent<Rigidbody2D>();
        body.velocity = Vector2.zero;
        body.angularVelocity = 0.0f;

        //just copying this piece of code from EnemyControl but should put in a function
        Vector3 dir = Statics.Player.localPosition - transform.localPosition;
        dir = dir.normalized;
        float force = 100f * body.mass;
        body.AddForce(dir * force, ForceMode2D.Impulse);
      
    }

    private void OnCollisionEnter2D(Collision2D objeto)
    {
        if (objeto.gameObject.tag == "ShadowShot")
        {
            Destroy(objeto.gameObject);//just erase them
        }

        if (objeto.gameObject.tag == "Shot")
        {
            Destroy(objeto.gameObject);
            if (Neo)//the idea is to avoid the shots from the player, but if it still collides we dont suffer damage
                return;

            _life.TakesLife(Statics.Damage);
            Instantiate(Explosion, transform).transform.parent = transform.parent;
        }
    }


    public void MyDeath()
    {
        //Explosao;
        if(MyType == Statics.TYPE_ENEMY.METEOR)
        {

            //if the randon value has to be larger than 60, doesnt that mean 40% chance?
            //I'm not sure what this code is doing but that seems weird
            //also
            //thats not how statistics work
            //if you take any group of values (say 0 to 100, to make percentages easy)
            //and assume a uniform distribution
            //then yes, values over a certain point will represent a correspondent percentage.
            //but THE CHOICE of which values doesnt affect the probability (again, assuming uniform)
            //you can pick values from 0-20 AND 80-100 and nothing inbetween
            //and still have the same percentage as picking all values over 60
            //again I'm not sure what this part of the code is even supposed to do, 
            //but this description seemed innacurate, unless I'm missing something
            //I guess theres a difference between each meteor having a 60% chance of exploding
            //And 60% of meteors exploding, and to me this comment lead to think of the 2nd case
            if (UnityEngine.Random.Range(0, 100) > 60)//60% chance of it becoming bits and pieces
            {
                Create(1);
                Create(2);
                Create(3);
                Create(4);
            }
            Statics.EnemiesDead++;
            
        }
        if(ItemDrop != null && UnityEngine.Random.Range(0, 100) > 70)
        {
            Instantiate(ItemDrop, transform.parent).transform.localPosition = transform.localPosition;
        }
        Statics.Points += Damage * 100;
        StartCoroutine(KillMe());
    }

    private IEnumerator KillMe()
    {
        yield return new WaitForSeconds(0.1f);
        Destroy(gameObject);
    }

    private void Create(int v)
    {
        
        GameObject goMunus = Instantiate(gameObject, transform.parent);
        goMunus.GetComponent<Enemy>().MyType = Statics.TYPE_ENEMY.PIECES;
        float scale = UnityEngine.Random.Range(0.2f, 0.6f);
        goMunus.transform.localScale = new Vector3(scale, scale, scale);
        float force = 100f * goMunus.GetComponent<Rigidbody2D>().mass;
        switch (v) {
           case 1: goMunus.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.down * force, ForceMode2D.Impulse);
                break;
            case 2:
                goMunus.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.up * force, ForceMode2D.Impulse);
                break;
            case 3:
                goMunus.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.left * force, ForceMode2D.Impulse);
                break;
            case 4:
                goMunus.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.right * force, ForceMode2D.Impulse);
                break;
        }
    }


    private IEnumerator Shoot()
    {
        //Pode executar até 3 tiros simultaneos
        while (true)
        {
            //Bad name since we are dividing
            //But Since we are reducing the amount of time passed to WaitForSeconds
            //We are effectively increasing the shooting speed
            yield return new WaitForSeconds(shot.ShootingPeriod / Statics.SpeedMultiplier);
            
                Statics.Damage = shot.Damage;
                GameObject goShooter = Instantiate(shot.Prefab, Vector3.zero, Quaternion.identity);
                goShooter.tag = "EnemyShot";
                goShooter.transform.parent = transform;
                goShooter.transform.localPosition = shot.Weapon.transform.localPosition;
                goShooter.GetComponent<Rigidbody2D>().AddForce(transform.up * ((shot.SpeedShooter * 12000f) * Time.deltaTime), ForceMode2D.Impulse);
                goShooter.AddComponent<BoxCollider2D>();
                goShooter.transform.parent = transform.parent;

                if (shot.TypeShooter == Statics.TYPE_SHOT.DOUBLE)
                {
                    GameObject goShooter2 = Instantiate(shot.Prefab, Vector3.zero, Quaternion.identity);
                    goShooter2.tag = "EnemyShot";
                    goShooter2.transform.parent = transform;
                    goShooter2.transform.localPosition = shot.Weapon2.transform.localPosition;
                    goShooter2.GetComponent<Rigidbody2D>().AddForce(transform.up * ((shot.SpeedShooter * 12000f) * Time.deltaTime), ForceMode2D.Impulse);
                    goShooter2.AddComponent<BoxCollider2D>();
                    goShooter2.transform.parent = transform.parent;
                }

                if (shot.TypeShooter == Statics.TYPE_SHOT.TRIPLE)
                {
                    GameObject goShooter2 = Instantiate(shot.Prefab, Vector3.zero, Quaternion.identity);
                    goShooter2.tag = "EnemyShot";
                    goShooter2.transform.parent = transform;
                    goShooter2.transform.localPosition = shot.Weapon2.transform.localPosition;
                    goShooter2.GetComponent<Rigidbody2D>().AddForce(transform.up * ((shot.SpeedShooter * 12000f) * Time.deltaTime), ForceMode2D.Impulse);
                    goShooter2.AddComponent<BoxCollider2D>();
                    goShooter2.transform.parent = transform.parent;

                    GameObject goShooter3 = Instantiate(shot.Prefab, Vector3.zero, Quaternion.identity);
                    goShooter3.tag = "EnemyShot";
                    goShooter3.transform.parent = transform;
                    goShooter3.transform.localPosition = shot.Weapon3.transform.localPosition;
                    goShooter3.GetComponent<Rigidbody2D>().AddForce(transform.up * ((shot.SpeedShooter * 12000f) * Time.deltaTime), ForceMode2D.Impulse);
                    goShooter3.AddComponent<BoxCollider2D>();
                    goShooter3.transform.parent = transform.parent;
                }
            

            
        }
    }

    public void EndBoss()
    {
        GameObject.Find("Control").GetComponent<ControlGame>().LevelPassed();
        Destroy(gameObject);
        Invoke("PStick", 1f);
    }

    void PStick()
    {

        if(Stick.GetStck() == Stick.stck.MANESTIC)
        {
            Update();
        }
    }
}
